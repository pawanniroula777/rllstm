import torch
import torch.nn as nn
import torch.nn.functional as F


class LSTM(nn.Module):
  def __init__(self, state_size, action_size, seed, hidden_dim, num_layers):
    super(LSTM, self).__init__()
    self.hidden_dim = hidden_dim
    self.num_layers = num_layers
    self.lstm = nn.LSTM(state_size, hidden_dim, num_layers, batch_first=True)
    self.fc = nn.Linear(hidden_dim, action_size)

  def forward(self,x):
    #Initialize hidden layer and cell state
    h0 = torch.zeros(self.num_layers, x.size(0), self.hidden_dim).requires_grad_()
    c0 = torch.zeros(self.num_layers, x.size(0), self.hidden_dim).requires_grad_()

    out, (hn, cn) = self.lstm(x, (h0.detach(), c0.detach()))
    out = self.fc(out[:, -1, :])
    m = torch.sigmoid(out)
    return m